package com.maxim.jms.adapter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;



@Component
public class ConsumerAdapter {

	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());
	
	public void sendToMongo(String json) {
		logger.info("Sending to MongoDb...."+json);
		
		MongoClient client = new MongoClient();
		DB db = client.getDB("vendor");
		//client.getDatabase("vendor");
		DBCollection collection = db.getCollection("contact");
		logger.info("Converting JSON to DBObject ....");
		DBObject dbObject = (DBObject)JSON.parse(json);
		collection.insert(dbObject);
		logger.info("Sent JSON to MongoDb...");
		
	}

}
