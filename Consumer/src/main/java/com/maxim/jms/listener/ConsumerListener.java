package com.maxim.jms.listener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.maxim.jms.adapter.ConsumerAdapter;

@Component
public class ConsumerListener implements MessageListener {

	private static Logger logger = LogManager.getLogger(ConsumerListener.class.getName());
	@Autowired
	JmsTemplate jmsTemplate; 
	
	@Autowired
	ConsumerAdapter consumerAdapter;
	
	@Override
	public void onMessage(Message message) {
		logger.info("In onMessage in consumer listener....");
		String json=null;
		
		if(message instanceof TextMessage){
			
			try{
				
				json=((TextMessage)message).toString();
				logger.info("Sending json to mongodb.."+json);
				consumerAdapter.sendToMongo(json);
			} catch(Exception e){
				logger.error("Error Sending json to mongodb.."+json);
				jmsTemplate.convertAndSend(json);
			}
		}
				
	}

}
